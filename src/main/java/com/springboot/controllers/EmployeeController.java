package com.springboot.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springboot.models.Employee;
import com.springboot.services.EmployeeService;

@Controller
public class EmployeeController {
@Autowired
EmployeeService employeeService;

private List<Employee> employees = new ArrayList<Employee>();
private Employee employee= new Employee();

@RequestMapping(value = { "/", "/index"}, method = RequestMethod.GET)
public String index(Model model) {
	
	model.addAttribute("message", "Bonjour AJC");
	
	return "index";
}
	@RequestMapping(value= {"/employee"}, method = RequestMethod.GET)
	public String employee( Model model) {
		this.employees=employeeService.findAll();
		for (Employee employee : employees) {
			System.out.println(employees);
		}
		model.addAttribute("employee", employees);	
		return "employee";
	}
	
	@RequestMapping(value= {"/employee/detail/{id}"}, method = RequestMethod.GET)
public String employeeDetail(Model model,@PathVariable(name="id") Integer id) {
		this.employee=employeeService.findById(id);
		model.addAttribute("emp", employee);
		model.addAttribute("modecreate", false);
		return "addEmployee";
}

}
