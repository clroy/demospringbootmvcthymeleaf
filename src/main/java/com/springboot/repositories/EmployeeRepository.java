package com.springboot.repositories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.springboot.models.Employee;

@Repository
public class EmployeeRepository {
@Autowired
JdbcTemplate jdbcTemplate;

public List<Employee> findAll() {
	return jdbcTemplate.query("select * from Employee", new BeanPropertyRowMapper<Employee>(Employee.class));
}

public Employee findById(int id) {
	return jdbcTemplate.queryForObject("SELECT ID, LOGIN, EMAIL, PASSWORD,  PRENOM, NOM, ROLE FROM EMPLOYEE WHERE ID = ? ", BeanPropertyRowMapper.newInstance(Employee.class), id);
}

public void updateEmployee(Employee employe) {
    jdbcTemplate.update("UPDATE employee SET  login = ?, password = ?, nom = ?, prenom = ?, email = ?, role = ? WHERE ID = ? ", new Object[] {employe.getLogin(), employe.getPassword(),
            employe.getNom(), employe.getPrenom(), employe.getEmail(),  employe.getRole(), employe.getId()});
}
}
