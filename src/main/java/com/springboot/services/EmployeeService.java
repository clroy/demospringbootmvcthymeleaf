package com.springboot.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.models.Employee;
import com.springboot.repositories.EmployeeRepository;

@Service
public class EmployeeService {
@Autowired
EmployeeRepository _employeeRepository;

public List<Employee> findAll(){
	return _employeeRepository.findAll();
}

public Employee findById(int id) {
	return _employeeRepository.findById(id);
}

public void updateEmployee(Employee employe) {
	_employeeRepository.updateEmployee(employe);
}


}
